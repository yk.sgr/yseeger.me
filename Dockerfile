FROM node:15-alpine

RUN mkdir -p /opt/website
WORKDIR /opt/website

RUN apk update && apk upgrade
RUN apk add git

COPY . /opt/website
RUN npm install

RUN npm run build

ENV NUXT_HOST=0.0.0.0
EXPOSE 3000

CMD ["npm", "start"]